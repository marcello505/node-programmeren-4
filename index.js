const express = require("express");
const bodyParser = require("body-parser");
const studenthomeRoutes = require("./src/routes/studenthome.routes");
const config = require("./src/config/config");

const app = express();
const port = process.env.PORT || 3000;
const logger = config.logger;

app.use(bodyParser.json());

app.use("/api", studenthomeRoutes);

app.all("*", (req, res) => res.status(404).send("No endpoint found"));

app.listen(port, () =>
    logger.info(`Example app listening at http://localhost:${port}`)
);

module.exports = app;
