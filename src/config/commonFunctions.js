const logger = require("./config").logger;

module.exports = {
    handleError(message, res, error, TAG) {
        logger.error(TAG, error);
        res.status(400).json({
            message: message,
            error: error,
        });
    },
    handleUserError(message, res, error) {
        res.status(400).json({
            message: message,
            error: error,
        });
    },
};
