const logLevel = process.env.LOGLEVEL || "trace";

module.exports = {
    logger: require("tracer").console({
        level: logLevel,
    }),
    dbconfig: {
        host: process.env.DB_HOST || "localhost",
        user: process.env.DB_USER || "studenthome_user",
        database: process.env.DB_DATABASE || "studenthome",
        password: process.env.DB_PASSWORD || "secret",
        connectionLimit: 20,
    },
    jwtKey: "mvhaddem",
};
