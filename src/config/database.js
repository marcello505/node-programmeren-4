const mysql = require("mysql");
const logger = require("./config").logger;
const dbconfig = require("./config").dbconfig;

const pool = mysql.createPool(dbconfig);

module.exports = pool;

// pool.getConnection((err, connection) => {
//   if (err) throw err; // not connected!

//   // Use the connection
//   connection.query("SELECT * FROM studenthome", (error, results, fields) => {
//     // When done with the connection, release it.
//     connection.release();

//     // Handle error after the release.
//     if (error) throw error;

//     console.log("results: ", results);
//     // Don't use the connection here, it has been returned to the pool.
//   });
// });
