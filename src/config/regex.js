module.exports = {
    postalcode: new RegExp(/^[0-9]{4}[ ]{0,1}[a-zA-Z]{2}$/),
    telephone: new RegExp(/^[+]{0,1}[0-9]{1,3}[-]{0,1}[0-9]*$/),
    date: new RegExp(/^[0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}$/),
    email: new RegExp(/^\w*@\w*.\w*$/),
};
