const config = require("../config/config");
const logger = config.logger;
const assert = require("assert");
const pool = require("../config/database");
const jwt = require("jsonwebtoken");
const regex = require("../config/regex");
const commonFunctions = require("../config/commonFunctions");

let controller = {
    getToken(req, res) {
        const TAG = "getToken";
        const { email, password } = req.body;
        let sqlQuery = "SELECT * FROM user WHERE Email = ? AND Password = ?";
        try {
            assert.match(
                email,
                regex.email,
                "Field 'email' didn't pass the regex or wasn't a string. Make sure it's properly formatted. Example: 'John@Doe.com'"
            );
            assert(
                typeof password === "string",
                "Field 'password' is missing or not a string"
            );
        } catch (err) {
            commonFunctions.handleUserError(err.message, res, err);
            return;
        }

        //Connectie met de database
        pool.getConnection(function (err, connection) {
            if (err)
                commonFunctions.handleError(
                    TAG + " failed getting connection!",
                    res,
                    err,
                    TAG
                );

            if (connection) {
                connection.query(
                    sqlQuery,
                    [email, password],
                    (err, results, fields) => {
                        if (err)
                            commonFunctions.handleError(
                                TAG + " failed calling query",
                                res,
                                err,
                                TAG
                            );
                        if (results) {
                            if (results[0] == undefined) {
                                commonFunctions.handleUserError(
                                    "User doesn't exist",
                                    res,
                                    err
                                );
                                return;
                            }
                            jwt.sign(
                                { userId: results[0].ID },
                                config.jwtKey,
                                {
                                    expiresIn: "48h",
                                },
                                function (err, token) {
                                    if (err) {
                                        commonFunctions.handleError(
                                            TAG + " has failed signing a token",
                                            res,
                                            err,
                                            TAG
                                        );
                                    }

                                    if (token) {
                                        res.status(200).json({
                                            result: {
                                                message: "Login succesful",
                                                token: token,
                                                results,
                                            },
                                        });
                                    }
                                }
                            );
                        }
                    }
                );
            }
        });
    },
    makeUser(req, res) {
        const TAG = "makeUser";
        const {
            email,
            password,
            firstname,
            lastname,
            studentnumber,
        } = req.body;
        let sqlQuery =
            "INSERT INTO user (First_Name, Last_Name, Email, Student_Number, Password) VALUES (?, ?, ?, ?, ?)";
        try {
            assert.match(
                email,
                regex.email,
                "Field 'email' didn't pass the regex or wasn't a string. Make sure it's properly formatted. Example: 'John@Doe.com'"
            );
            assert(
                typeof password === "string",
                "Field 'password' is missing or not a string"
            );
            assert(
                typeof firstname,
                "Field 'firstname' is missing or not a string"
            );
            assert(
                typeof lastname,
                "Field 'lastname' is missing or not a string"
            );
            assert(
                typeof studentnumber,
                "Field 'studentnumber' is missing or not a string"
            );
        } catch (err) {
            commonFunctions.handleUserError(err.message, res, err);
            return;
        }

        //Connectie met de database
        pool.getConnection(function (err, connection) {
            if (err)
                commonFunctions.handleError(
                    TAG + " failed getting connection!",
                    res,
                    err,
                    TAG
                );

            if (connection) {
                connection.query(
                    sqlQuery,
                    [firstname, lastname, email, studentnumber, password],
                    (err, results, fields) => {
                        if (err)
                            commonFunctions.handleError(
                                TAG + " failed calling query",
                                res,
                                err,
                                TAG
                            );
                        if (results) {
                            jwt.sign(
                                { userId: results.insertId },
                                config.jwtKey,
                                {
                                    expiresIn: "48h",
                                },
                                function (err, token) {
                                    if (err) {
                                        commonFunctions.handleError(
                                            TAG + " has failed signing a token",
                                            res,
                                            err,
                                            TAG
                                        );
                                    }

                                    if (token) {
                                        res.status(200).json({
                                            result: {
                                                message: "Register succesful",
                                                token: token,
                                                results,
                                            },
                                        });
                                    }
                                }
                            );
                        }
                    }
                );
            }
        });
    },
    validateToken(req, res, next) {
        const TAG = "validateToken";
        var token = req.headers.authorization;
        if (!token) {
            logger.warn("Authorization header missing");
            res.status(401).json({
                error: "Authorization header missing",
            });
        }

        token = token.substring(7, token.length);
        jwt.verify(token, config.jwtKey, (err, payload) => {
            if (err) {
                commonFunctions.handleError("Not authroized", res, err, TAG);
            }

            if (payload) {
                logger.debug(TAG, "token is valid", payload);
                req.userId = payload.userId;
                req.body.UserID = payload.userId;
                next();
            }
        });
    },
};

module.exports = controller;
