const config = require("../config/config");
const logger = config.logger;
const assert = require("assert");
const pool = require("../config/database");
const regex = require("../config/regex");
const commonFunctions = require("../config/commonFunctions");

let controller = {
    validateMeal(req, res, next) {
        const meal = req.body;
        try {
            assert(
                typeof meal.Name === "string",
                'Field "Name" is missing or not a string'
            );
            assert(
                typeof meal.Description === "string",
                'Field "Description" is missing or not a string'
            );
            assert(
                typeof meal.Ingredients === "string",
                'Field "Ingredients" is missing or not a string'
            );
            assert(
                typeof meal.Allergies === "string",
                'Field "Allergies" is missing or not a string'
            );
            assert.match(
                req.body.CreatedOn,
                regex.date,
                "CreatedOn didn't pass the regex. Make sure it's properly formatted. Example: 'yyyy-mm-dd'"
            );
            assert.match(
                req.body.OfferedOn,
                regex.date,
                "CreatedOn didn't pass the regex. Make sure it's properly formatted. Example: 'yyyy-mm-dd'"
            );
            assert(
                typeof meal.Price === "number",
                'Field "Price" is missing or not a number'
            );
            assert(
                typeof meal.UserID === "number",
                'Field "UserID" is missing or not a number'
            );
            assert(
                typeof meal.MaxParticipants === "number",
                'Field "MaxParticipants" is missing or not a number'
            );

            next();
        } catch (err) {
            commonFunctions.handleUserError("Error adding meal", res, err);
        }
    },
    createMeal(req, res) {
        const TAG = "createMeal";
        const meal = req.body;
        let sqlQuery =
            "INSERT INTO meal (Name, Description, Ingredients, Allergies, CreatedOn, OfferedOn, Price, UserID, StudenthomeID, MaxParticipants) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        logger.debug(TAG, "sqlQuery =", sqlQuery);

        pool.getConnection(function (err, connection) {
            if (err) {
                commonFunctions.handleError(
                    TAG + " failed getting connection!",
                    res,
                    err,
                    TAG
                );
            }

            if (connection) {
                // Use the connection
                connection.query(
                    sqlQuery,
                    [
                        meal.Name,
                        meal.Description,
                        meal.Ingredients,
                        meal.Allergies,
                        meal.CreatedOn,
                        meal.OfferedOn,
                        meal.Price,
                        meal.UserID,
                        req.params.homeId,
                        meal.MaxParticipants,
                    ],
                    (error, results, fields) => {
                        connection.release();
                        if (error) {
                            commonFunctions.handleError(
                                TAG + " failed calling query",
                                res,
                                error,
                                TAG
                            );
                        }
                        if (results) {
                            logger.trace("Results: ", results);
                            res.status(200).json({
                                result: {
                                    id: results.insertId,
                                    ...meal,
                                },
                            });
                        }
                    }
                );
            }
        });
    },
    getMeal(req, res) {
        const TAG = "getMeal";
        const homeId = req.params.homeId;

        let sqlQuery = "SELECT * FROM meal WHERE StudenthomeID = ?";
        logger.debug(TAG, "sqlQuery =", sqlQuery);

        // Maak de connectie
        pool.getConnection(function (err, connection) {
            if (err) {
                commonFunctions.handleError(
                    TAG + " failed getting connection!",
                    res,
                    err,
                    TAG
                );
            }
            if (connection) {
                // Use connection
                connection.query(sqlQuery, [homeId], (err, results, fields) => {
                    connection.release();
                    if (err) {
                        commonFunctions.handleError(
                            TAG + " failed calling query",
                            res,
                            err,
                            TAG
                        );
                    }

                    if (results) {
                        logger.trace("Results: ", results);
                        res.status(200).json({
                            result: results,
                        });
                    }
                });
            }
        });
    },
    getMealByMealId(req, res) {
        const TAG = "getMealByMealId";
        logger.info(TAG, "called");
        const homeId = req.params.homeId;
        const mealId = req.params.mealId;
        let sqlQuery = "SELECT * FROM meal WHERE StudenthomeID = ? AND ID = ?";

        // Maak de connectie
        pool.getConnection(function (err, connection) {
            if (err) {
                commonFunctions.handleError(
                    TAG + " failed getting connection!",
                    res,
                    err,
                    TAG
                );
            }
            if (connection) {
                connection.query(
                    sqlQuery,
                    [homeId, mealId],
                    (err, results, fields) => {
                        connection.release();
                        if (err) {
                            commonFunctions.handleError(
                                TAG + " failed calling query",
                                res,
                                err,
                                TAG
                            );
                        }
                        if (results) {
                            if (results[0] == undefined) {
                                commonFunctions.handleUserError(
                                    "No record found",
                                    res,
                                    err
                                );
                                return;
                            }
                            logger.trace("Results: ", results);
                            res.status(200).json({
                                result: results,
                            });
                        }
                    }
                );
            }
        });
    },
    putMealById(req, res) {
        const TAG = "putMealById";
        logger.info(TAG, "called");
        const mealId = req.params.mealId;
        const homeId = req.params.homeId;
        let sqlQuery =
            "SELECT * FROM meal WHERE ID = ? AND StudenthomeID = ? AND UserID = ?";

        // Dates checken met de Regex
        if (req.body.CreatedOn != undefined) {
            try {
                assert.match(
                    req.body.CreatedOn,
                    regex.date,
                    "CreatedOn didn't pass the regex. Make sure it's properly formatted. Example: 'yyyy-mm-dd'"
                );
            } catch (err) {
                commonFunctions.handleUserError(
                    "Date did not pass the regex. Make sure it's formatted like this: 1970-01-01",
                    res,
                    err
                );
                return;
            }
        }
        if (req.body.OfferedOn != undefined) {
            try {
                assert.match(
                    req.body.OfferedOn,
                    regex.date,
                    "CreatedOn didn't pass the regex. Make sure it's properly formatted. Example: 'yyyy-mm-dd'"
                );
            } catch (err) {
                commonFunctions.handleUserError(
                    "Date did not pass the regex. Make sure it's formatted like this: 1970-01-01",
                    res,
                    err
                );
                return;
            }
        }

        // Maak de connectie
        pool.getConnection(function (err, connection) {
            if (err) {
                commonFunctions.handleError(
                    TAG + " failed getting connection!",
                    res,
                    err,
                    TAG
                );
            }

            if (connection) {
                connection.query(
                    sqlQuery,
                    [mealId, homeId, req.body.UserID],
                    (err, results, fields) => {
                        if (err) {
                            commonFunctions.handleError(
                                TAG + " failed calling query",
                                res,
                                err,
                                TAG
                            );
                        }
                        if (results) {
                            logger.debug(TAG, results);
                            if (results[0] == undefined) {
                                commonFunctions.handleUserError(
                                    "Could not find record. Does it exist? Do you have access to delete it?",
                                    res,
                                    err
                                );
                                return;
                            }
                            let result = {
                                ...results[0],
                                ...req.body,
                            };
                            logger.trace("PUT request: ", result);
                            sqlQuery =
                                "UPDATE meal SET Name = ?, Description = ?, Ingredients = ?, Allergies = ?, CreatedOn = ?, OfferedOn = ?, Price = ?, UserID = ?, StudenthomeID = ?, MaxParticipants = ?" +
                                "\nWHERE ID = ? AND StudenthomeID = ?";
                            connection.query(
                                sqlQuery,
                                [
                                    result.Name,
                                    result.Description,
                                    result.Ingredients,
                                    result.Allergies,
                                    result.CreatedOn,
                                    result.OfferedOn,
                                    result.Price,
                                    result.UserID,
                                    result.StudenthomeID,
                                    result.MaxParticipants,
                                    mealId,
                                    homeId,
                                ],
                                (err, results, fields) => {
                                    connection.release();
                                    if (err) {
                                        commonFunctions.handleError(
                                            TAG + " failed calling query",
                                            res,
                                            err,
                                            TAG
                                        );
                                    }

                                    if (results) {
                                        logger.trace("Results: ", results);
                                        res.status(200).json({
                                            results,
                                        });
                                    }
                                }
                            );
                        }
                    }
                );
            }
        });
    },
    deleteMealById(req, res) {
        const TAG = "deleteMealById";
        logger.info(TAG, "called");
        const mealId = req.params.mealId;
        const homeId = req.params.homeId;
        sqlQuery =
            "DELETE FROM meal WHERE ID = ? AND StudenthomeID = ? AND UserID = ?";

        // Maak de connectie
        pool.getConnection(function (err, connection) {
            if (err) {
                commonFunctions.handleError(
                    TAG + " failed getting connection!",
                    res,
                    err,
                    TAG
                );
            }
            if (connection) {
                connection.query(
                    sqlQuery,
                    [mealId, homeId, req.body.UserID],
                    (err, results, fields) => {
                        connection.release();
                        if (err) {
                            commonFunctions.handleError(
                                TAG + " failed calling query",
                                res,
                                err,
                                TAG
                            );
                        }
                        if (results) {
                            if (results.affectedRows == 0) {
                                commonFunctions.handleUserError(
                                    "No meal record found",
                                    res,
                                    err
                                );
                            } else {
                                logger.trace("Results: ", results);
                                res.status(200).json({
                                    result: results,
                                });
                            }
                        }
                    }
                );
            }
        });
    },
};

module.exports = controller;
