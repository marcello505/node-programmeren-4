const config = require("../config/config");
const logger = config.logger;
const assert = require("assert");
const pool = require("../config/database");
const regex = require("../config/regex");
const commonFunctions = require("../config/commonFunctions");

let controller = {
    validateStudenthome(req, res, next) {
        const studentHome = req.body;
        try {
            assert(
                typeof studentHome.Name === "string",
                'Field "Name" is missing or not a string'
            );
            assert(
                typeof studentHome.Address === "string",
                'Field "Address" is missing or not a string'
            );
            assert(
                typeof studentHome.House_Nr === "number",
                'Field "House_Nr" is missing or not a number'
            );
            assert(
                typeof studentHome.UserID === "number",
                'Field "UserID" is missing or not a number'
            );
            assert.match(
                studentHome.Postal_Code,
                regex.postalcode,
                "Postal_Code didn't pass the regex or wasn't a string. Make sure it's properly formatted. Example: '2413SB'"
            );
            assert.match(
                studentHome.Telephone,
                regex.telephone,
                "Telephone didn't pass the regex or wasn't a string. Make sure it's completly numeric. Only exception being a + sign at the start. Example: '+31612345678'"
            );
            assert(
                typeof studentHome.City === "string",
                'Field "City" is missing or not a string'
            );
            next();
        } catch (err) {
            commonFunctions.handleUserError(err.message, res, err);
        }
    },
    createStudenthome(req, res) {
        const TAG = "createStudenthome";
        logger.info("createStudenthome called");
        const studentHome = req.body;
        let sqlQuery =
            "INSERT INTO studenthome (Name, Address, House_Nr, UserID, Postal_Code, Telephone, City) VALUES (?, ?, ?, ?, ?, ?, ?)";
        logger.debug("createStudenthome", "sqlQuery =", sqlQuery);

        pool.getConnection(function (err, connection) {
            if (err)
                commonFunctions.handleError(
                    "createStudenthome failed getting connection!",
                    res,
                    err,
                    TAG
                );

            if (connection) {
                // Use the connection
                connection.query(
                    sqlQuery,
                    [
                        studentHome.Name,
                        studentHome.Address,
                        studentHome.House_Nr,
                        studentHome.UserID,
                        studentHome.Postal_Code,
                        studentHome.Telephone,
                        studentHome.City,
                    ],
                    (error, results, fields) => {
                        connection.release();
                        if (error)
                            commonFunctions.handleError(
                                "createStudenthome failed calling query",
                                res,
                                error,
                                TAG
                            );

                        if (results) {
                            logger.trace("Results: ", results);
                            res.status(200).json({
                                result: {
                                    id: results.insertId,
                                    ...studentHome,
                                },
                            });
                        }
                    }
                );
            }
        });
    },
    getStudenthome(req, res) {
        const TAG = "getStudenthome";
        const name = req.query.name;
        const city = req.query.city;
        let sqlQuery = "";

        // Maak de SQLquery
        if (name === undefined && city === undefined) {
            sqlQuery = "SELECT * FROM studenthome";
        } else if (name != undefined && city === undefined) {
            sqlQuery = "SELECT * FROM studenthome WHERE Name = '" + name + "'";
        } else if (name === undefined && city != undefined) {
            sqlQuery = "SELECT * FROM studenthome WHERE City = '" + city + "'";
        } else if (name && city) {
            sqlQuery =
                "SELECT * FROM studenthome WHERE City = '" +
                city +
                "' AND Name = '" +
                name +
                "'";
        }
        logger.debug("getStudenthome", "sqlQuery =", sqlQuery);

        // Maak de connectie
        pool.getConnection(function (err, connection) {
            if (err) {
                commonFunctions.handleError(
                    "getStudenthome failed getting connection!",
                    res,
                    err,
                    TAG
                );
            }
            if (connection) {
                // Use connection
                connection.query(sqlQuery, (err, results, fields) => {
                    connection.release();
                    if (err) {
                        commonFunctions.handleError(
                            "getStudenthome failed calling query",
                            res,
                            err,
                            TAG
                        );
                    }
                    if (results) {
                        if (results[0] == undefined) {
                            commonFunctions.handleUserError(
                                "No records found",
                                res,
                                err
                            );
                            return;
                        }
                        res.status(200).json({
                            result: results,
                        });
                    }
                });
            }
        });
    },
    getStudenthomeById(req, res) {
        const TAG = "getStudenthomeById";
        logger.info(TAG, "called");
        const id = req.params.homeId;
        let sqlQuery = "SELECT * FROM studenthome WHERE ID = ?";

        // Maak de connectie
        pool.getConnection(function (err, connection) {
            if (err) {
                commonFunctions.handleError(
                    "getStudenthomeById failed getting connection!",
                    res,
                    err,
                    TAG
                );
            }
            if (connection) {
                connection.query(sqlQuery, [id], (err, results, fields) => {
                    connection.release();
                    if (err) {
                        commonFunctions.handleError(
                            "getStudenthomeById failed calling query",
                            res,
                            err,
                            TAG
                        );
                    }
                    if (results) {
                        if (results[0] == undefined) {
                            commonFunctions.handleUserError(
                                "No records found",
                                res,
                                err
                            );
                            return;
                        }
                        logger.trace("Results: ", results);
                        res.status(200).json({
                            result: results,
                        });
                    }
                });
            }
        });
    },
    putStudenthomeById(req, res) {
        const TAG = "putStudenthomeById";
        logger.info(TAG, "called");
        const id = req.params.homeId;
        sqlQuery = "SELECT * FROM studenthome WHERE ID = ? AND UserID = ?";

        // Telephone en Postal_Code checken met de Regex
        if (req.body.Postal_Code != undefined) {
            try {
                assert.match(
                    studentHome.Postal_Code,
                    regex.postalcode,
                    "Postal_Code didn't pass the regex or wasn't a string. Make sure it's properly formatted. Example: '2413SB'"
                );
            } catch (err) {
                commonFunctions.handleUserError(
                    "Postal_Code did not pass the regex",
                    res,
                    err
                );
                return;
            }
        }
        if (req.body.Telephone != undefined) {
            try {
                assert.match(
                    studentHome.Telephone,
                    regex.telephone,
                    "Telephone didn't pass the regex or wasn't a string. Make sure it's completly numeric. Only exception being a + sign at the start. Example: '+31612345678'"
                );
            } catch (err) {
                commonFunctions.handleUserError(
                    "Telephone did not pass the regex",
                    res,
                    err
                );
                return;
            }
        }

        // Maak de connectie
        pool.getConnection(function (err, connection) {
            if (err) {
                commonFunctions.handleError(
                    "putStudenthomeById failed getting connection!",
                    res,
                    err,
                    TAG
                );
            }

            if (connection) {
                connection.query(
                    sqlQuery,
                    [id, req.body.UserID],
                    (err, results, fields) => {
                        if (err) {
                            commonFunctions.handleError(
                                "putStudenthomeById failed calling query.",
                                res,
                                err,
                                TAG
                            );
                        }

                        if (results) {
                            logger.debug(TAG, results);
                            if (results[0] == undefined) {
                                commonFunctions.handleUserError(
                                    "Could not find record. Does it exist? Do you have access to delete it?",
                                    res,
                                    err
                                );
                                return;
                            }
                            let result = {
                                ...results[0],
                                ...req.body,
                            };
                            logger.trace("PUT request: ", result);
                            sqlQuery =
                                "UPDATE studenthome SET Name = ?, Address = ?, House_Nr = ?, UserID = ?, Postal_Code = ?, Telephone = ?, City = ?" +
                                "\nWHERE ID = " +
                                id;
                            connection.query(
                                sqlQuery,
                                [
                                    result.Name,
                                    result.Address,
                                    result.House_Nr,
                                    result.UserID,
                                    result.Postal_Code,
                                    result.Telephone,
                                    result.City,
                                ],
                                (err, results, fields) => {
                                    connection.release();
                                    if (err) {
                                        commonFunctions.handleError(
                                            "putStudenthomeById failed calling query.",
                                            res,
                                            err,
                                            TAG
                                        );
                                    }
                                    if (results) {
                                        logger.trace("Results: ", results);
                                        res.status(200).json({
                                            results,
                                        });
                                    }
                                }
                            );
                        }
                    }
                );
            }
        });
    },
    deleteStudenthomeById(req, res) {
        const TAG = "deleteStudenthomeById";
        logger.info(TAG, "called");
        const id = req.params.homeId;
        sqlQuery = "DELETE FROM studenthome WHERE ID = ? AND UserID = ?";

        // Maak de connectie
        pool.getConnection(function (err, connection) {
            if (err) {
                commonFunctions.handleError(
                    "deleteStudenthomeById failed getting connection!",
                    res,
                    err,
                    TAG
                );
            }

            if (connection) {
                connection.query(
                    sqlQuery,
                    [id, req.body.UserID],
                    (err, results, fields) => {
                        connection.release();
                        if (err) {
                            commonFunctions.handleError(
                                "failed calling query",
                                res,
                                err,
                                TAG
                            );
                        }

                        if (results) {
                            if (results.affectedRows == 0) {
                                commonFunctions.handleUserError(
                                    "No record found or no permission.",
                                    res,
                                    err
                                );
                                return;
                            } else {
                                logger.trace("Results: ", results);
                                res.status(200).json({
                                    result: results,
                                });
                            }
                        }
                    }
                );
            }
        });
    },
};

module.exports = controller;
