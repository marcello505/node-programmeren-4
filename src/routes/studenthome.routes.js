const express = require("express");
const router = express.Router();
const authenticateController = require("../controllers/authenticate.controller");
const studenthomeController = require("../controllers/studenthome.controller");
const mealController = require("../controllers/meal.controller");
const config = require("../config/config");
const logger = config.logger;

// UC-10X routes
router.post("/register", authenticateController.makeUser);
router.post("/login", authenticateController.getToken);
router.get("/info", (req, res) => {
    logger.info("Get request op /api/info");
    const result = {
        result: true,
        naam: "Marcello Haddeman",
        studentenNummer: 2152991,
        beschrijving:
            "Dit is een server systeem voor het managen van een SQL database via verschillende http requests.",
        URL: "https://programmeren-4-marcello.herokuapp.com",
    };
    res.status(200).json(result);
});
router.post("*", authenticateController.validateToken);
router.put("*", authenticateController.validateToken);
router.delete("*", authenticateController.validateToken);

// UC-20X routes
router.post("/studenthome", studenthomeController.validateStudenthome);

router.post("/studenthome", studenthomeController.createStudenthome);

router.get("/studenthome", studenthomeController.getStudenthome);

router.get("/studenthome/:homeId", studenthomeController.getStudenthomeById);

router.put("/studenthome/:homeId", studenthomeController.putStudenthomeById);

router.delete(
    "/studenthome/:homeId",
    studenthomeController.deleteStudenthomeById
);

// UC-30X routes
router.post("/studenthome/:homeId/meal", mealController.validateMeal);

router.post("/studenthome/:homeId/meal", mealController.createMeal);

router.put("/studenthome/:homeId/meal/:mealId", mealController.putMealById);

router.get("/studenthome/:homeId/meal", mealController.getMeal);

router.get("/studenthome/:homeId/meal/:mealId", mealController.getMealByMealId);

router.delete(
    "/studenthome/:homeId/meal/:mealId",
    mealController.deleteMealById
);

module.exports = router;
