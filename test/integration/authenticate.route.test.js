var assert = require("assert");
const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../index");

chai.should();
chai.use(chaiHttp);

describe("UC-10X Testcases", function () {
    describe("#TC-101-X", function () {
        it("TC-101-1 Verplicht veld ontbreekt", function (done) {
            chai.request(server)
                .post("/api/register")
                .send({
                    firstname: "John",
                    lastname: "Doe",
                    email: "john@doe.com",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-101-2 Invalide email adres", function (done) {
            chai.request(server)
                .post("/api/register")
                .send({
                    firstname: "John",
                    lastname: "Doe",
                    email: "john",
                    studentnumber: "333333",
                    password: "secret",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-101-3 Invalide wachtwoord", function (done) {
            chai.request(server)
                .post("/api/register")
                .send({
                    firstname: "John",
                    lastname: "Doe",
                    email: "john@doe.com",
                    studentnumber: "333333",
                    password: 123123,
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-101-4 Gebruiker bestaat al", function (done) {
            chai.request(server)
                .post("/api/register")
                .send({
                    firstname: "Jan",
                    lastname: "Smit",
                    emal: "jsmit@server.nl",
                    studentnumber: "222222",
                    password: "secret",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-101-5 Gebruiker succesvol geregistreerd", function (done) {
            chai.request(server)
                .post("/api/register")
                .send({
                    firstname: "John",
                    lastname: "Doe",
                    email: "john@doe.com",
                    studentnumber: "444444",
                    password: "secret",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(200);
                    done();
                });
        });
    });
    describe("#TC-102-X", function () {
        it("TC-102-1 Verplicht veld ontbreekt", function (done) {
            chai.request(server)
                .post("/api/login")
                .send({
                    email: "john@doe.com",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-102-2 Invalide email adres", function (done) {
            chai.request(server)
                .post("/api/login")
                .send({
                    email: "john",
                    password: "secret",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-102-3 Invalide wachtwoord", function (done) {
            chai.request(server)
                .post("/api/login")
                .send({
                    email: "john@doe.com",
                    password: 123123,
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-102-4 Gebruiker bestaat niet", function (done) {
            chai.request(server)
                .post("/api/login")
                .send({
                    email: "jane@doe.com",
                    password: "secret",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-102-5 Gebruiker succesvol ingelogd", function (done) {
            chai.request(server)
                .post("/api/login")
                .send({
                    email: "john@doe.com",
                    password: "secret",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(200);
                    done();
                });
        });
    });
});
