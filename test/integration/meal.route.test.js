var assert = require("assert");
const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../index");

chai.should();
chai.use(chaiHttp);

let token = "";
let insertedID;

describe("UC-30X Testcases", function () {
    before(function (done) {
        chai.request(server)
            .post("/api/login")
            .send({
                email: "john@doe.com",
                password: "secret",
            })
            .end((err, res) => {
                token = res.body.result.token;
                console.log("TC-30X Token: " + token);
                done();
            });
    });
    describe("#TC-301-X", function () {
        it("TC-301-1 Verplicht veld ontbreekt", function (done) {
            chai.request(server)
                .post("/api/studenthome/1/meal")
                .set("Authorization", "Bearer " + token)
                .send({
                    Name: "Broodje Kaas",
                    Ingredients: "Brood, Kaas",
                    Allergies: "Glutose",
                    CreatedOn: "2020-05-03",
                    OfferedOn: "2020-05-03",
                    Price: 20,
                    UserID: 1,
                    StudenthomeID: 1,
                    MaxParticipants: 1,
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-301-2 Niet ingelogd", function (done) {
            chai.request(server)
                .post("/api/studenthome/1/meal")
                .send({
                    Name: "Broodje Kaas",
                    Description: "Een lekker broodje kaas",
                    Ingredients: "Brood, Kaas",
                    Allergies: "Glutose",
                    CreatedOn: "2020-05-03",
                    OfferedOn: "2020-05-03",
                    Price: 20,
                    UserID: 1,
                    StudenthomeID: 1,
                    MaxParticipants: 1,
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(401);
                    done();
                });
        });
        it("TC-301-3 Maaltijd succesvol toegevoegd", function (done) {
            chai.request(server)
                .post("/api/studenthome/1/meal")
                .set("Authorization", "Bearer " + token)
                .send({
                    Name: "Broodje Kaas",
                    Description: "Een lekker broodje kaas",
                    Ingredients: "Brood, Kaas",
                    Allergies: "Glutose",
                    CreatedOn: "2020-05-03",
                    OfferedOn: "2020-05-03",
                    Price: 20,
                    UserID: 1,
                    StudenthomeID: 1,
                    MaxParticipants: 1,
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(200);
                    insertedID = res.body.result.id;
                    console.log("InsertedID=" + insertedID);
                    done();
                });
        });
    });
    describe("#TC-302-X", function () {
        it("TC-302-1 Verplicht veld ontbreekt", function (done) {
            chai.request(server)
                .put("/api/studenthome/1/meal/" + insertedID)
                .set("Authorization", "Bearer " + token)
                .send({
                    Name: "Broodje Worst",
                    CreatedOn: "aaa",
                    OfferedOn: "aaa",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-302-2 Niet ingelogd", function (done) {
            chai.request(server)
                .put("/api/studenthome/1/meal/" + insertedID)
                .send({
                    Name: "Broodje Worst",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(401);
                    done();
                });
        });

        it("TC-302-3 Niet de eigenaar van de data", function (done) {
            chai.request(server)
                .put("/api/studenthome/1/meal/1")
                .set("Authorization", "Bearer " + token)
                .send({
                    Name: "Broodje Worst",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-302-4 Maaltijd bestaat niet", function (done) {
            chai.request(server)
                .put("/api/studenthome/1/meal/0")
                .set("Authorization", "Bearer " + token)
                .send({
                    Name: "Broodje Worst",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-302-5 Maaltijd succesvol gewijzigd", function (done) {
            chai.request(server)
                .put("/api/studenthome/1/meal/" + insertedID)
                .set("Authorization", "Bearer " + token)
                .send({
                    Name: "Broodje Worst",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(200);
                    done();
                });
        });
    });
    describe("#TC-303-X", function () {
        it("TC-303-1 Lijst van maaltijden geretourneerd", function (done) {
            chai.request(server)
                .get("/api/studenthome/1/meal")
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(200);
                    done();
                });
        });
    });
    describe("#TC-304-X", function () {
        it("TC-304-1 Maaltijd bestaat niet", function (done) {
            chai.request(server)
                .get("/api/studenthome/1/meal/0")
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-304-2 Details van maaltijd geretourneerd", function (done) {
            chai.request(server)
                .get("/api/studenthome/1/meal/" + insertedID)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(200);
                    done();
                });
        });
    });
    describe("#TC-305-X", function () {
        it("TC-305-1 Verplicht veld ontbreekt", function (done) {
            chai.request(server)
                .delete("/api/studenthome/1/meal/aaa")
                .set("Authorization", "Bearer " + token)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-305-2 Niet ingelogd", function (done) {
            chai.request(server)
                .delete("/api/studenthome/1/meal/" + insertedID)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(401);
                    done();
                });
        });

        it("TC-305-3 Niet de eigenaar van de data", function (done) {
            chai.request(server)
                .delete("/api/studenthome/1/meal/1")
                .set("Authorization", "Bearer " + token)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-305-4 Maaltijd bestaat niet", function (done) {
            chai.request(server)
                .delete("/api/studenthome/1/meal/0")
                .set("Authorization", "Bearer " + token)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-305-", function (done) {
            chai.request(server)
                .delete("/api/studenthome/1/meal/" + insertedID)
                .set("Authorization", "Bearer " + token)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(200);
                    done();
                });
        });
    });
});
