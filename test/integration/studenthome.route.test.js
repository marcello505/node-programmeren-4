var assert = require("assert");
const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../index");
const pool = require("../../src/config/database");
chai.should();
chai.use(chaiHttp);

let token = "";
let insertedID;

describe("UC-20X Testcases", function () {
    before(function (done) {
        chai.request(server)
            .post("/api/login")
            .send({
                email: "john@doe.com",
                password: "secret",
            })
            .end((err, res) => {
                token = res.body.result.token;
                console.log("TC-20X Token: " + token);
                done();
            });
    });
    describe("#TC-201-X", function () {
        it("TC-201-1 Verplicht veld ontbreekt", function (done) {
            chai.request(server)
                .post("/api/studenthome")
                .set("Authorization", "Bearer " + token)
                .send({
                    Name: "De Vlinders",
                    Address: "Roetingsstraat",
                    House_Nr: 21,
                    City: "Breda",
                    Telephone: "06123456891",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-201-2 Invalide postcode", function (done) {
            chai.request(server)
                .post("/api/studenthome")
                .set("Authorization", "Bearer " + token)
                .send({
                    Name: "De Vlinders",
                    Address: "Roetingsstraat",
                    House_Nr: 21,
                    Postal_Code: "123456",
                    City: "Breda",
                    Telephone: "06123456891",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-201-3 Invalide telefoonnummer", function (done) {
            chai.request(server)
                .post("/api/studenthome")
                .set("Authorization", "Bearer " + token)
                .send({
                    Name: "De Vlinders",
                    Address: "Roetingsstraat",
                    House_Nr: 21,
                    Postal_Code: "123456",
                    City: "Breda",
                    Telephone: 123123123,
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-201-4 Studentenhuis bestaat al", function (done) {
            chai.request(server)
                .post("/api/studenthome")
                .set("Authorization", "Bearer " + token)
                .send({
                    Name: "De Rotweilers",
                    Address: "Roetingsstraat",
                    House_Nr: 21,
                    Postal_Code: "1234BRR",
                    City: "Breda",
                    Telephone: "0612345678",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-201-5 Niet ingelogd", function (done) {
            chai.request(server)
                .post("/api/studenthome")
                .send({
                    Name: "De Rotweilers",
                    Address: "Roetingsstraat",
                    House_Nr: 21,
                    Postal_Code: "1234BR",
                    City: "Breda",
                    Telephone: "0612345678",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(401);
                    done();
                });
        });

        it("TC-201-6 Studentenhuis succesvol toegevoegd", function (done) {
            chai.request(server)
                .post("/api/studenthome")
                .set("Authorization", "Bearer " + token)
                .send({
                    Name: "De Rotweilers",
                    Address: "Roetingsstraat",
                    House_Nr: 21,
                    Postal_Code: "1234BR",
                    City: "Breda",
                    Telephone: "0612345678",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(200);
                    insertedID = res.body.result.id;
                    console.log("InsertedID=" + insertedID);
                    done();
                });
        });
    });
    describe("#TC-202-X", function () {
        it("TC-202-1 Toon nul studentenhuizen", function (done) {
            chai.request(server)
                .get("/api/studenthome")
                .set("Authorization", "Bearer " + token)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(200);
                    done();
                });
        });
        it("TC-202-2 Toon twee studentenhuizen", function (done) {
            chai.request(server)
                .get("/api/studenthome")
                .set("Authorization", "Bearer " + token)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(200);
                    done();
                });
        });
        it("TC-202-3 Toon studentenhuizen met zoekterm op niet-bestaande stad", function (done) {
            chai.request(server)
                .get("/api/studenthome?city=qwertyazerty")
                .set("Authorization", "Bearer " + token)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-202-4 Toon studentenhuizen met zoekterm op niet-bestaande naam", function (done) {
            chai.request(server)
                .get("/api/studenthome?name=qwertyazerty")
                .set("Authorization", "Bearer " + token)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-202-5 Toon studentenhuizen met zoekterm op bestaande stad", function (done) {
            chai.request(server)
                .get("/api/studenthome?city=Breda")
                .set("Authorization", "Bearer " + token)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(200);
                    done();
                });
        });

        it("TC-202-6 Toon studentenhuizen met zoekterm op bestaande naam", function (done) {
            chai.request(server)
                .get("/api/studenthome?name=De Rotweilers")
                .set("Authorization", "Bearer " + token)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(200);
                    done();
                });
        });
    });
    describe("#TC-203-X", function () {
        it("TC-203-1 Studentenhuis-ID bestaat niet", function (done) {
            chai.request(server)
                .get("/api/studenthome/0")
                .set("Authorization", "Bearer " + token)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-203-2 Studentenhuis-ID bestaat", function (done) {
            chai.request(server)
                .get("/api/studenthome/1")
                .set("Authorization", "Bearer " + token)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(200);
                    done();
                });
        });
    });
    describe("#TC-204-X", function () {
        it("TC-204-1 Verplicht veld ontbreekt", function (done) {
            chai.request(server)
                .put("/api/studenthome/test")
                .set("Authorization", "Bearer " + token)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-204-2 Invalide postcode", function (done) {
            chai.request(server)
                .put("/api/studenthome/" + insertedID)
                .set("Authorization", "Bearer " + token)
                .send({
                    Postal_Code: "123456",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-204-3 Invalide telefoonnummer", function (done) {
            chai.request(server)
                .put("/api/studenthome/" + insertedID)
                .set("Authorization", "Bearer " + token)
                .send({
                    Telephone: "blablabla",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-204-4 Studentenhuis bestaat niet", function (done) {
            chai.request(server)
                .put("/api/studenthome/0")
                .set("Authorization", "Bearer " + token)
                .send({
                    Name: "De Vlinders",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-204-5 Niet ingelogd", function (done) {
            chai.request(server)
                .put("/api/studenthome/" + insertedID)
                .send({
                    Name: "De Vlinders",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(401);
                    done();
                });
        });

        it("TC-204-6 Studentenhuis succesvol gewijzigd", function (done) {
            chai.request(server)
                .put("/api/studenthome/" + insertedID)
                .set("Authorization", "Bearer " + token)
                .send({
                    Name: "De Vlinders",
                })
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(200);
                    done();
                });
        });
    });
    describe("#TC-205-X", function () {
        it("TC-205-1 Studentenhuis bestaat niet", function (done) {
            chai.request(server)
                .delete("/api/studenthome/0")
                .set("Authorization", "Bearer " + token)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });

        it("TC-205-2 Niet ingelogd", function (done) {
            chai.request(server)
                .delete("/api/studenthome/" + insertedID)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(401);
                    done();
                });
        });
        it("TC-205-3 Actor is geen eigenaar", function (done) {
            chai.request(server)
                .delete("/api/studenthome/1")
                .set("Authorization", "Bearer " + token)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(400);
                    done();
                });
        });
        it("TC-205-4 Studentenhuis succesvol verwijderd", function (done) {
            chai.request(server)
                .delete("/api/studenthome/" + insertedID)
                .set("Authorization", "Bearer " + token)
                .end((err, res) => {
                    res.should.be.an("object");
                    res.should.have.status(200);
                    done();
                });
        });
    });
    after(function (done) {
        pool.getConnection((err, connection) => {
            if (connection) {
                connection.query(
                    "DELETE FROM user WHERE Email = 'john@doe.com'",
                    (err, results, fields) => {
                        connection.release();
                        done();
                    }
                );
            }
        });
    });
});
